const fs = require('fs');
const input = fs.createReadStream('./input.txt');
const output = fs.createWriteStream('./output.txt');
const crypto = require('crypto');
const Transform = require('stream').Transform;

class Hash extends Transform {
  constructor(algorythm, options) {
    super(options);
    this.hash = crypto.createHash(algorythm);
  }

  _transform(chunk, encoding, callback) {
    this.hash.update(chunk);
    callback();
  }

  _flush(callback) {
    this.push(this.hash.digest('hex'));
    callback();
  }
}


const hash = new Hash('md5');

input.pipe(hash).pipe(process.stdout);
input.pipe(hash).pipe(output);
