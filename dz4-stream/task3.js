const stream = require('stream');
const Transform = require('stream').Transform;
const Readable = require('stream').Readable;
const Writable = require('stream').Writable;

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

class RandomGenerator extends Readable {
  _read() {
    this.push(getRandomInt(-1024, 1024).toString());
  }
}

class Outputter extends Writable {
  _write(chunk, encoding, callback) {
    console.log(chunk.toString());
    callback();
  }
}

class Converter extends Transform {
  _transform(chunk, encoding, callback) {
    setTimeout( () => {
      callback(null, chunk.toString('hex'));
    }, 1000 * 1);
  }
}

const randomGenerator = new RandomGenerator();
const outputter = new Outputter();
const converter = new Converter();

randomGenerator.pipe(converter).pipe(outputter);
