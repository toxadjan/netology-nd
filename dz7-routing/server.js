const express = require('express');
const app = express();
const PORT = process.env.PORT || 3000;
const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({'extended': true}));


app.get('/', (req, res) => {
  res.status(200).send('Hello Express.js!');
});

app.get('/hello', (req, res) => {
  res.status(200).send('Hello stranger!');
});

app.get('/hello/:name', (req, res) => {
  res.status(200).send(`Hello, ${req.params.name}!`);
});

app.all('/sub/*/*', (req, res) => {
  res.status(200).send(`You requested URI: ${req.url}`);
});

const checkRequest = (req, res, next) => {
  if (Object.keys(req.body).length === 0) {
    res.status(404).send('404 Not Found');
    return next( Error('POST request empty') );
  }
  next();
};

const checkHeaders = (req, res, next) => {
  if (!req.get('Key')) {
    res.sendStatus(401);
    return next( Error('No Key Header Provided') );
  }
  next();
};

app.post('/post', checkRequest, checkHeaders, (req, res) => {
  res.json(req.body);
});

app.use((err, req, res, next) => {
  if (res.headersSent) {
      return next(err);
  }
  res.status(err.status || 500);
  res.render('error', {
      message: err.message,
      error: {}
  });
});

app.listen(PORT,  () => console.log(`Server listening on port ${PORT}`));
