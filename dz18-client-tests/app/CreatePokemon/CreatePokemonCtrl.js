'use strict';

angular
    .module('myApp')
    .controller('CreatePokemonCtrl', function($scope) {

    let vm = this;
    vm.created = false;

    vm.newPokemon = {};
    vm.addPokemon = function(myPokemon) {
        console.log(myPokemon);
        $scope.newPokemon = myPokemon;
        vm.created = true;
        vm.newPokemon = {};
        vm.pokemonForm.$setPristine();

    };

});
