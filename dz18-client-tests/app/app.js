'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ui.router',
    'ngMessages'
]).
config(function($stateProvider) {

    $stateProvider
        .state({
            name: 'list',
            url: '/list',
            templateUrl: 'PokemonList/PokemonList.html',
            controller: 'PokemonListCtrl as vm'
        })
        .state({
            name: 'createNewPokemon',
            url: '/new',
            templateUrl: 'CreatePokemon/CreatePokemon.html',
            controller: 'CreatePokemonCtrl as vm'
        })
        .state({
            name: 'detail',
            url: '/pokemons/:pokemonId',
            templateUrl: 'PokemonDetail/PokemonDetail.html',
            controller: 'PokemonDetailCtrl as vm'
        }).state({
            name: 'detail.edit',
            url: '/edit',
            parent: 'detail',
            templateUrl: 'PokemonDetail/PokemonDetailEdit.html',
            controller: function() {}
        }).state({
            name: 'dashboard',
            url: '/dashboard',
            templateUrl: 'Dashboard/Dashboard.html',
            controller: function($scope) {
              console.log('dashboardCtrl');
                $scope.login = function(form) {
                  console.log(form.userName);
                  $scope.dashboard.$setPristine(true);
                }
            }
        });
}).
controller('MainMenuCtrl', ['$scope', '$state', 'AuthService', function($scope, $state, AuthService) {
    let vm = this;
    $scope.$state = $state;
    $scope.$on('login', function(event, data){
      vm.name = AuthService.getUserData().name;
    });
    $scope.$on('logout', function(event, data){
      vm.name = 'Гость';
    });

}])
.component('authComponent', {
    templateUrl: 'AuthComponent/AuthComponent.html',
    controller: function($scope, AuthService) {
        $scope.login = function(form) {
            AuthService.login(form);
        };
        this.userData = AuthService.getUserData();
        $scope.logout = function() {
              AuthService.logout();
        };
    }
})
.factory('AuthService', function($state, $rootScope) {
    const state = {
        userData: {}
    };

    return {
        login(form)  {
            console.log('LOGIN');
            state.userData = {
              name: form.userName.$modelValue,
              email: form.userEmail.$modelValue,
              phone: form.userPhone.$modelValue
            }
            $rootScope.$broadcast('login');
            $state.reload();
        },
        getUserData() {
            return state.userData;
        },
        userLoggedIn() {
            if (state.userData.name) return true;
        },
        logout() {
            state.userData = {};
            $rootScope.$broadcast('logout');
            $state.reload();
        }
    };
});
// .run(function($rootScope, $state, AuthService) {
//     $rootScope.$on('$locationChangeStart', function() {
//         if (!AuthService.userLoggedIn()) {
//             $state.go('dashboard');
//         }
//     });
// });
