'use strict';

angular
    .module('myApp')
    .component('authComponent', {
        templateUrl: 'AuthComponent/AuthComponent.html',
        controller: function() {
            console.log('authComponent ctrl');
        }
    });
