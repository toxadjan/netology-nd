'use strict';

angular
    .module('myApp')
    .factory('AuthService', function($state) {
        const state = {
            userData: {}
        };

        return {
            login(form)  {
                console.log('LOGIN');
                state.userData = {
                  name: form.userName.$modelValue,
                  email: form.userEmail.$modelValue,
                  phone: form.userPhone.$modelValue
                }
                $state.go('list');
            },
            getUserData() {
                return state.userData;
            },
            userLoggedIn() {
                if (state.userData.name) return true;
            },
            logout() {
                state.userData = {};
                $state.reload();
            }


        };
    })
