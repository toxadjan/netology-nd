let PokemonListPage = function() {
  let self = this;
  let url = 'http://localhost:8000/#!/list';

  let pokemonList = element.all(by.repeater('singlePokemon in vm.pokemons'));

  self.get = function() {
    browser.get(url);
  };

  self.getPokemonList = function() {
    return pokemonList;
  };
};

module.exports = PokemonListPage;
