let DashboardPage = function() {
  let self = this;
  let url = 'http://localhost:8000/#!/dashboard';

  let dashboardButton = element(by.id('dashboardButton'));
  let authForm = element(by.name('dashboard'));
  let userName = element(by.model('user.name'));


  self.get = function() {
    browser.get(url);
  };

  self.setUserName = function(number) {
    userName.sendKeys(number);
  };

  self.getFormClass = function() {
    return authForm.getAttribute('class');
  };

  self.getForm = function () {
    return authForm;
  };

  self.getNavButtonClass = function() {
    return dashboardButton.getAttribute('class');
  };

};

module.exports = DashboardPage;
