let CreatePokemonPage = function() {
  let self = this;
  let url = 'http://localhost:8000/#!/new';

  let pokemonName = element(by.model('vm.newPokemon.name'));
  let pokemonWeight = element(by.model('vm.newPokemon.weight'));
  let addButton = element(by.id('addPokemon'));
  let newPokemonForm = element(by.name('vm.pokemonForm'));

  self.get = function() {
    browser.get(url);
  };

  self.setPokemonName = function(number) {
    pokemonName.sendKeys(number);
  };

  self.setPokemonWeight = function(number) {
    pokemonWeight.sendKeys(number);
  };

  self.addPokemon = function() {
    addButton.click();
  };

  self.getAlert = function() {
    let alertMessage = element(by.id('creationAlert')).getText();
    return alertMessage;
  };

  self.getFormClass = function() {
    return newPokemonForm.getAttribute('class');
  };
};

module.exports = CreatePokemonPage;
