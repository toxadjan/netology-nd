let PokemonAppHomepage = function() {
  let self = this;

  let url = 'http://localhost:8000/#!/';

  let pageTitle = browser.getTitle();

  self.get = function() {
    browser.get(url);
  };

  self.getPageTitle = function() {
    return pageTitle;
  };
};

module.exports = PokemonAppHomepage;
