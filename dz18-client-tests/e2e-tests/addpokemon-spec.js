let CreatePokemonPage = require('./page-objects/addpokemon');

describe('Pokemon Creation Page', function() {
  let pokemonApp = {};

  beforeEach(function() {
    pokemonApp = new CreatePokemonPage();
    pokemonApp.get();
  });

  it('should return alert message when pokemon was added', function() {
    pokemonApp.setPokemonName('Pikapika');
    pokemonApp.setPokemonWeight(100);
    pokemonApp.addPokemon();

    let alertMessage = pokemonApp.getAlert();
    expect(alertMessage).toContain('Pikapika');
  });

  it('form should be invalid when input is incorrect', function() {
    pokemonApp.setPokemonWeight(100);
    let formClass = pokemonApp.getFormClass();

    expect(formClass).toContain('ng-invalid');
  });

});
