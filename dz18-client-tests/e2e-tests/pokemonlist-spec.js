let PokemonListPage = require('./page-objects/pokemonlist');

describe('PokemonList Page', function() {
  let pokemonApp = {};

  beforeEach(function() {
    pokemonApp = new PokemonListPage();
    pokemonApp.get();
  });

  it('should get pokemon list', function() {
    let pokemonList = pokemonApp.getPokemonList();

    expect(pokemonList.length).not.toEqual(0);
  });
});
