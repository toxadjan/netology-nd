let PokemonAppHomepage = require('./page-objects/homepage');

describe('PokemonApp Homepage', function() {
  let pokemonApp = {};

  beforeEach(function() {
    pokemonApp = new PokemonAppHomepage();
    pokemonApp.get();
  });

  it('should have a correct title', function() {
    let title = pokemonApp.getPageTitle();

    expect(title).toEqual('My Pokemon AngularJS App');
  });
});
