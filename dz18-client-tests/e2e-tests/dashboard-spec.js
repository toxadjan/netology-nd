let DashboardPage = require('./page-objects/dashboard');

describe('Pokemon Dashboard Page', function() {
  let pokemonApp = {};

  beforeEach(function() {
    pokemonApp = new DashboardPage();
    pokemonApp.get();
  });

  it('should highlight current url navigation menu button', function() {
    let buttonClass = pokemonApp.getNavButtonClass();
    expect(buttonClass).toContain('active');
  });

  it('should have an auth form on this page', function() {
    let form = pokemonApp.getForm();
    expect(form).not.toEqual(null);
  });

  it('form should be invalid when input is incorrect', function() {
    pokemonApp.setUserName(100);
    let formClass = pokemonApp.getFormClass();

    expect(formClass).toContain('ng-invalid');
  });

});
