const chai = require('chai');
const sinon = require('sinon');
const assert = chai.assert;

const Pokemon = require('../Pokemon').Pokemon;
const PokemonList = require('../Pokemon').PokemonList;

const name = 'Pikachu';
const level = 100;

describe('Pokemon Class', () => {
  let pokemon = new Pokemon(name, level);

  describe('show() method', () => {
    it('should return correct log', () => {
      let spy = sinon.spy(console, 'log');
      pokemon.show();
      assert(spy.calledWith(`Hi! My name is ${name}, my level is ${level}`), 'returns incorrect log');
      spy.restore();
    });
  });

  describe('valueOf Method', () => {
    it('should return current level', () => {
      assert.equal(level, pokemon.valueOf());
    });
  });
});

describe('PokemonList Class', () => {
  let list = new PokemonList();

  before(() => {
    for (var i = 0; i < 10; i++) {
      list.add('Pokemon' + i, i * 10);
    }
  });
  after(() => {
    list = new PokemonList();
  });

  describe('add() method', () => {
    it('should create new Pokemon and add it to the list', () => {
      list.add(name, level);
      assert.instanceOf(list[list.length - 1], Pokemon);
    });
  });

  describe('show() method', () => {
    it('should return log with list.length', () => {
      let spy = sinon.spy(console, 'log');
      list.show();
      assert(spy.calledWith(`There are ${list.length} pokemons here.`), 'returns incorrect length');
      spy.restore();
    });
  });

  describe('max() method', () => {
    it('should return item from list array with max level', () => {
      list.max();
      console.log(list.max());
      assert.equal(level, list.max());
    });
  });

});
