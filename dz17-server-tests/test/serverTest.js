const chai = require('chai');
const supertest = require('supertest');
const expect = chai.expect;
const assert = chai.assert;

describe('RESTful API', () => {
  let server;

  before((done) => {
    require('../server');
    setTimeout(() => {
      server = supertest.agent('http://localhost:3000');
      done();
    }, 1000);
  });

  describe('RESTful /users/', function() {
    let URL = '/rest/users/';
    const user = {
      name: 'Dj',
      score: 71
    };

    it('Should create user & return status 200', function(done) {
      server
        .post(URL).send(user).expect(function(res) {
          assert.equal(res.status, 200);
        })
        .end(done);
    });

    it('Should delete user & return status 200', function(done) {
      server
        .delete(URL + '1').expect(function(res) {
          assert.equal(res.status, 200, 'user not found');
        })
        .end(done);
    });
  });
});
