const express = require('express');
const app = express();

const {rest} = require('./rest');
const PORT = process.env.PORT || 3000;

app.use('/rest', rest);

app.all('*', (req, res) => {
  res.json('Please use /rest for REST api or /rpc for RPC api');
});

app.listen(PORT,  () => console.log(`Server listening on port ${PORT}`));
