const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');
const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({'extended': true}));

const checkRequest = (req, res, next) => {
  if (!req.body || Object.keys(req.body).length === 0) {
    res.status(400).send('incorrect BODY query');
    return next( Error( 'incorrect BODY query' ) );
  }
  next();
};

const checkUser = (req, res, next) => {
  db.forEach(item => {
    if (item.name === req.body.name) {
      res.status(400).send(`user ${req.body.name} already exist`);
      return next( Error( 'user exists' ) );
    }
  });
  next();
};

const checkId = (req, res, next) => {
  let user = db.find((item) => item.id == req.params.id);
  if (!user) {
    res.status(400).send(`user with id '${req.params.id}' not found`);
    return next( Error( 'user not found' ) );
  }
  next();
};

router.route('/users/')
  .get((req, res) => {
    console.log('new GET request', req.url);
    let limit = req.query.limit || db.length;
    let offset = req.query.offset || 0;
    let result = db.slice(offset, limit);
    res.status(200).json(result);
  })
  .post(checkRequest, checkUser, (req, res) => {
    console.log('new POST request', req.url, req.body);
    let {name, score} = req.body;
    let user = {name, score};
    user.id = db.length + 1;
    db.push(user);
    res.json(`user ${name} created`).status(200);
  });

router.route('/users/:id')
  .get(checkId, (req, res) => {
    console.log('new GET request', req.url, req.params);
    let user = db.find((item) => item.id === req.params.id);
    res.json(user).status(200);
  })
  .put(checkRequest, checkId, (req, res) => {
    db.map((item) => {
      if (item.id == req.params.id) {
        item.name = req.body.name || item.name;
        item.score = req.body.score || item.score;
      }
    });
    res.json(db).status(200);
  })
  .delete(checkId, (req, res) => {
    db.map((item, index) => {
      if (item.id == req.params.id) {
        db.splice(index, 1);
      }
    });
    res.json(`user with id ${req.params.id} removed`).status(200);
  });

  router.delete('/delete-users', (req, res) => {
    db.splice(0, db.length);
    res.status(200).json('all users deleted');
  });

  router.use((err, req, res, next) => {
      if (res.headersSent) {
          return next(err);
      }
      res.status(err.status || 500);
      res.render('error', {
          message: err.message,
          error: {}
      });
  });

exports.rest = router;
