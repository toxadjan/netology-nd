const express = require('express');
const app = express();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + '/public'));

io.on('connection', (socket) => {
  console.log('connect');
  var username, room;

  socket.on('login', (data) => {
    username = data.username;
    room = data.room;
    socket.join(room);
    socket.to(room).emit('user-joined', { username, room });
    socket.emit('login-success', { username, room });
  });

  socket.on('message', (data) => {
    room = data.room;
    io.to(room).emit('message', {
      username: data.username,
      message: data.message
    });
  });

  socket.on('disconnect', (data) => {
    console.log('disconnect', username);
    if (username) {
      io.to(room).emit('message', {
        username: username,
        message: "has left chat"
      });
    }
  });
  
});

server.listen(PORT,  () => console.log(`Server listening at port ${PORT}`));
