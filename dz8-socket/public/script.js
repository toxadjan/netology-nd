$(document).ready(function() {
  var socket = io(),
      $window = $(window),
      username, room,
      $loginWrapper = $('#login-wrapper'),
      $loginForm = $('#login-form'),
      $username = $('#username'),
      $room = $('#room'),
      $chatWrapper = $('#chat-wrapper'),
      $chatHeader = $('#chat-header'),
      $chatBody = $('#chat-body'),
      $chatFormWrapper = $('#chat-form-wrapper'),
      $chatMessageForm = $('#chat-message-form'),
      $messageInput = $('#message-input'),
      $logoutBtn = $('#logout-btn'),
      $msgTpl = $('#msg-tpl');

  $loginForm.on('submit', function(e) {
    e.preventDefault();
    socket.emit('login', { username: $username.val(), room: $room.val() });
  });

  $chatMessageForm.on('submit', function(e) {
    e.preventDefault();
    var msg = $messageInput.val();
    if (!msg.length) return;
    socket.emit('message', { username: $username.val(), room: $room.val(), message: msg });
    $messageInput.val('');
  });

  $logoutBtn.on('click', function(e) {
    e.preventDefault();
    location.reload();
  });

  socket.on('connect', function() {

    socket.on('login-success', function(data) {
      console.log('login ok');

      $loginWrapper.fadeOut(function() {
        username = data.username;
        room = data.room;
        $chatHeader.find('h2').html('Welcome, ' + data.username + "! <small>" + (data.room || 'Main room' ) + '</small>');
        $chatWrapper.fadeIn();
      });
    });

    socket.on('message', function(data) {
      console.log('message', data);
      var tmp = $msgTpl.html();
      tmp = tmp
          .replace("::username::", data.username)
          .replace("::message::", data.message);
      $chatBody.append(tmp);
    });

    socket.on('user-joined', function(data) {
      var tmp = $msgTpl.html();
      tmp = tmp
          .replace("::username::", data.username)
          .replace("::message::", "has joined chat");
      $chatBody.append(tmp);
    });

    socket.on('disconnect', function () {
      location.reload();
    });

  });

});
