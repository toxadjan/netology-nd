'use strict';

pokemonApp.controller('BerryListCtrl', function($scope, BerryService) {
    // $scope.berriesLoaded = false;

    $scope.berries = BerryService.query();
    $scope.berries.$promise.then(function(){
        $scope.berriesLoaded = true;
    });
});
