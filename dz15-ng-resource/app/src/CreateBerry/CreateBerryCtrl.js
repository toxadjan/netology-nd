'use strict';

pokemonApp.controller('CreateBerryCtrl', function($scope, BerryService) {

    $scope.newBerry = {};

    $scope.createBerry = function(myBerry) {

        var newBerryInstance = new BerryService(myBerry);
        newBerryInstance.$save({}, function(successResult) {
            // Окей!
            $scope.newBerry = {};

            $scope.newBerryId = successResult.objectId;
            $scope.creationSuccess = true;
        }, function(errorResult) {
            // Не окей..
            $scope.creationSuccess = false;
        });

    };

});
