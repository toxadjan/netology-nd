var pokemonApp = angular.module('PokemonApp', ['ngRoute', 'ngResource']);

angular.
module('PokemonApp')

.config(['$routeProvider',
    function config($routeProvider) {

        $routeProvider.
        when('/pokemons', {
            templateUrl: 'src/PokemonList/PokemonList.html',
            controller: 'PokemonListCtrl'
        }).
        when('/pokemons/:pokemonId', {
            templateUrl: 'src/PokemonDetail/PokemonDetail.html',
            controller: 'PokemonDetailCtrl'
        }).
        when('/editPokemon/:pokemonId', {
            templateUrl: 'src/EditPokemon/EditPokemon.html',
            controller: 'EditPokemonCtrl'
        }).
        when('/createPokemon', {
            templateUrl: 'src/CreatePokemon/CreatePokemon.html',
            controller: 'CreatePokemonCtrl'
        }).
        when('/createBerry', {
            templateUrl: 'src/CreateBerry/CreateBerry.html',
            controller: 'CreateBerryCtrl'
        }).
        when('/berries', {
            templateUrl: 'src/BerryList/BerryList.html',
            controller: 'BerryListCtrl'
        }).
        when('/berries/:berryId', {
            templateUrl: 'src/BerryDetail/BerryDetail.html',
            controller: 'BerryDetailCtrl'
        }).
        when('/editBerry/:berryId', {
            templateUrl: 'src/EditBerry/EditBerry.html',
            controller: 'EditBerryCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
    }
])

.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.headers.common = {
        "application-id": "0D1546AF-A55E-DDDF-FFA5-B6AB7354BD00",
        "secret-key": "3FCD7B3D-454B-9DCD-FFCE-EA5E7FBE2900"
    };

}]);
