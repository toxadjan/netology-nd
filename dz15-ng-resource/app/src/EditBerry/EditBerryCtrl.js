'use strict';

pokemonApp.controller('EditBerryCtrl', function($scope, BerryService, $routeParams) {

    $scope.berry = BerryService.get({
        berryId: $routeParams['berryId']
    });

    $scope.updateBerry = function() {

        $scope.berry.$update({}, function(successResult) {
            // Окей!
            $scope.updateSuccess = true;
        }, function(errorResult) {
            // Не окей..
            $scope.updateSuccess = false;
        });

    }

});
