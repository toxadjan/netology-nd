'use strict';

pokemonApp.component('pokemonList', {

    controller: function PokemonListCtrl($scope, PokemonsService) {
        $scope.pokemonsLoaded = false;

        this.pokemons = PokemonsService.query( {}, function(successResult) {
            // Окей!
            $scope.pokemonsLoaded = true;

        }, function(errorResult) {
            // Не окей..
            $scope.pokemonsLoaded = true;

        });

    },

    templateUrl: './src/PokemonList/PokemonList.html'

});
