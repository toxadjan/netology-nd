'use strict';

pokemonApp.component('pokemonDetail', {
    controller: function PokemonDetailCtrl($scope, $routeParams, PokemonsService) {

        $scope.pokemon = PokemonsService.get({
            pokemonId: $routeParams['pokemonId']
        });

        $scope.pokemon.$promise.then(function(result) {
            $scope.pokemonLoaded = true;
        }).catch(function(error){
            console.log(error);
            $scope.notfoundError = true;
            $scope.pokemonLoaded = true;
        });

        $scope.deletePokemon = function(pokemonId) {
            $scope.pokemon.$delete({
                pokemonId: pokemonId
            }, function(successResult) {
                $scope.deletionSuccess = true;

            }, function(errorResult) {
                $scope.deletionError = true;
            });
        };
    },

    templateUrl: 'src/PokemonDetail/PokemonDetail.html',
    controllerAs: 'detailCtrl'
});
