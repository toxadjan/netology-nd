const EventEmitter = require("events");
class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();

(function moduleCommon (app) {
  let money = 100;

  app.on("common.update.wallet", (data) => {
    money += data;

    sendWallet();
  });

  function sendWallet() {
    console.log(money);
  }
})(myEmitter);

(function moduleGame1 (app) {
  setTimeout( () => {
    app.emit("common.update.wallet", Math.random() >= 0.5 ? 10 : -10);
  }, 1000 * 1);
})(myEmitter);

(function moduleGame2 (app) {
  setTimeout( () => {
    app.emit("common.update.wallet", Math.random() >= 0.5 ? 5 : -5);
  }, 1000 * 2);
})(myEmitter);

return;
