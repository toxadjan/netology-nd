const ChatApp = require('./chat');

let webinarChat =  new ChatApp('webinar');
let facebookChat = new ChatApp('=========facebook');
let vkChat =       new ChatApp('---------vk');

let chatOnMessage = (message) => {
  console.log(message);
};

function gettingReady() {
  console.log(`${this.title}: Готовлюсь к ответу`);
}

webinarChat
          .on('message', gettingReady)
          .on('message', chatOnMessage);

vkChat
      .setMaxListeners(2)
      .on('message', gettingReady)
      .on('message', chatOnMessage)
      .on('close', () => console.log('Чат вконтакте закрылся :(') )
      .close();

facebookChat.on('message', chatOnMessage);


// Закрыть вконтакте
setTimeout( ()=> {
  console.log('Закрываю вконтакте...');
  vkChat.removeListener('message', chatOnMessage);
}, 1000 * 10 );


// Закрыть фейсбук
setTimeout( ()=> {
  console.log('Закрываю фейсбук, все внимание — вебинару!');
  facebookChat.removeListener('message', chatOnMessage);
}, 1000 * 15 );


// Закрыть webinar
setTimeout( ()=> {
  webinarChat.emit('message', 'chatOnMessage');
  webinarChat.removeAllListeners();
  console.log('Webinar закрыт!');
}, 1000 * 30 );
