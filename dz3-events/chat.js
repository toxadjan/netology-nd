const EventEmitter = require('events');

class ChatApp extends EventEmitter {
  /**
   * @param {String} title
   */
  constructor(title) {
    super();

    this.title = title;

    // Посылать каждую секунду сообщение
    setInterval(() => {
      this.emit('message', `${this.title}: ping-pong`);
  }, 1000);
  }

  close() {
    this.emit('close');

    this.removeAllListeners(); //в данном случае по смыслу нужно удалять обработчики

    return this;
  }
}

module.exports = ChatApp;
