'use strict';
class Pokemon {
  constructor(name, level) {
    this.name = name,
    this.level = level;
  }
  show() {
    console.log('покемон %s - уровень %d', this.name, this.level);
  }
  valueOf() {
    return this.level;
  }
}

class PokemonList extends Array {
  constructor(pokemonListTitle, ...pokemons) {
    super();
    this.title = pokemonListTitle;
    for (let i = 0, imax = pokemons.length; i < imax; i+=2) {
      this.push (new Pokemon (pokemons[i], pokemons[i+1]) );
    }
  }
  add(name, level) {
    this.push(new Pokemon(name, level));
  }
  show() {
    console.info('\nСписок %s (%d покемонов):', this.title, this.length);
    this.forEach( (item) => item.show() );
  }
  movePokemon(pokemon, listToTransfer) {
    let pokemonIndex = this.findIndex( (item) => item.name === pokemon );
    if (~pokemonIndex) {
      listToTransfer.push(this[pokemonIndex]);
      this.splice(pokemonIndex, 1);
      console.info(`\nПокемон ${pokemon} перемещен в список ${listToTransfer.title} из списка ${this.title}`);
    } else {
        console.info(`\nПокемон ${pokemon} не найден в списке ${this.title}`);
    }
  }
  max() {
    let maxLevel = Math.max(...this);
    let topPokemon = this.find( (item) => item.level === maxLevel );

    return topPokemon;
  }
}

const lost = new PokemonList('lost', 'snorlax', 5, 'pikachu', 15, 'charmander', 10);
lost.add('psyduck', 100);
lost.show();

const found = new PokemonList('found');
found.add('bulbazaur', 55);
found.add('togepi', 1);
found.show();

lost.movePokemon('psyduck', found);
lost.show();
found.show();

found.max();
