const MongoClient = require('mongodb').MongoClient,
      assert = require('assert'),
      url = 'mongodb://localhost:27017/test';

const userList = [
  {
    name: 'John',
    lastname: 'Rambo',
    age: 69
  },
  {
    name: 'Phil',
    lastname: 'Smith'
  },
  {
    name: "Fox",
    lastname: "Mulder",
    age: 56
  },
  {
    name: "Bryan",
    lastname: "Perkins"
  }
];

MongoClient.connect(url, (err, db) => {
  assert.equal(null, err);
  console.log('Successfully connected to the MongoDB server');

  const collection = db.collection('heroes');

  collection.insertMany(userList).then(result => {
    console.log(`Inserted ${userList.length} documents into the collection`);

    collection.find().toArray().then(result => {
      console.log(result);
    });

    collection.update({ lastname: "Rambo" }, {'$set': {lastname: "Lennon"}}).then(result => {
      console.log('Changed Rambo lastname to Lennon');
    });

    collection.updateMany({ age: null }, { '$set': {age: 18} }).then(result => {
      console.log(`Set undefined age to default for ${result.modifiedCount} users`);
    });

    collection.find().toArray().then(result => {
      console.log(result);
    });

    collection.removeMany({ age: 18 }).then(result => {
      assert.equal(null, err);
      console.log(`Removed ${result.deletedCount} 18 y.o. users`);
      db.close();
    });
  })
  .catch(err => console.log(err));

});
