const express = require('express');
const bodyParser = require('body-parser');
const db = require('./db');
const router = express.Router();

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({'extended': true}));

const rpc = {
  getAll(params, callback) {
    callback(null, db);
  },

  get(params, callback) {
    const {id} = params;
    if (!id) return callback('ID not specified');
    let user = db.filter(item => item.id == id);

    return (!user.length) ? callback('user not found') : callback(null, user);
  },

  create(params, callback) {
    if (!params.name) return callback('Incorrect params');
    const user = params;
    user.id = db.length + 1;
    db.push(user);
    console.log('user created');
    callback(null, db);
  },

  update(params, callback) {
    if (!params.id) return callback('id not specified');
    let user = db.find((item) => item.id == params.id);
    if (!user) return callback('User not found');
    db.map((item) => {
      if (item.id == params.id) {
        item.name = params.name || item.name;
        item.score = params.score || item.score;
      }
    });
    console.log('user updated');
    callback(null, db);
  },

  delete(params, callback) {
    if (!params.id) return callback('id not specified');
    let user = db.find((item) => item.id == params.id);
    if (!user) return callback('User not found');
    db.map((item, index) => {
      if (item.id == params.id) {
        db.splice(index, 1);
      }
    });
    console.log('user deleted');
    callback(null, db);
  }
};

const responseHandler = {
  error(id, message, data) {
    return {jsonrpc: '2.0', error: {message, data}, id};
  },
  success(id, result) {
    return {jsonrpc: '2.0', result, id};
  }
};

function checkRequest(req, res, next) {
  if (req.body.jsonrpc !== '2.0' || !rpc[req.body.method] || !req.body.id) {
    res.status(400).send('incorrect rpc request');
    return next( Error( 'incorrect rpc request' ) );
  }
  next();
}

router.post('/', checkRequest, (req, res) => {
  const method = rpc[req.body.method];
  const id = req.body.id;
  method(req.body.params, (err, result) => {
    if (err) res.json(responseHandler.error(id, err));
    res.status(200).json(responseHandler.success(id, result));
  });
});

router.use((err, req, res, next) => {
    if (res.headersSent) {
        return next(err);
    }
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

exports.rpc = router;
