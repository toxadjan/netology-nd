# RPC #

- list all users
`{ "jsonrpc": "2.0", "method": "getAll", "id": 1 }`

- list specific user
`{ "jsonrpc": "2.0", "method": "get", "params": { "id": 1 }, "id": 1 }`

- create user
`{ "jsonrpc": "2.0", "method": "create", "params": { "name": "name", "score": 0 }, "id": 1 }`

- update user
`{ "jsonrpc": "2.0", "method": "update", "params": { "id": 1, "name": "name, "score": 0 }, "id": 1 }`

- delete user
`{ "jsonrpc": "2.0", "method": "delete", "params": { "id": 1 }, "id": 1 }`


### Описание задания:

Коллеги, всем добрый день!

Домашнее задание следующее:

Часть 1
• Создать Web-Server на express;

• Реализовать CRUD для сущности /users;

• Определить свойство name и score;

• Использовать REST;

• Реализовать обработку ошибок.

Часть 2
• Создать Web-Server на express;

• Реализовать CRUD для сущности /users;

• Определить свойство name и score;

• Использовать RPC;

• Реализовать обработку ошибок.



Дополнительное задание:

• Взять любую реализацию Web-Server`а (из задач выше);

• Реализовать возможность удалять всех пользователей;

• Реализовать limit, offset, fields .

Успехов!
