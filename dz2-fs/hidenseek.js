const fs = require('fs');
const fsPromise = require('./fs-promise');
const random = require('./random');
const shuffle = require('./shuffle');
const pokemons = require('./pokemons');
const Pokemon = require('./pokemon');
const PokemonList = require('./PokemonList');

const hide = (path, pokemons) => {
  // получаем список случайных покемонов (от 1 до 3)
  let hidden = shuffle(pokemons).slice(0, random(1, 3));

  return fsPromise.mkdir(path)
    .then( () => {
      for (let i = 1, imax = 10; i <= imax; ++i) {
        fsPromise.mkdir(`./${path}/${('0' + i).slice(-2)}`); // создаем 10 папок
      }
    })
    .then( () => console.log('Folders created'))
    .then( () => fsPromise.readdir(path))
    .then( (result) => {
      // выбираем для укрытия покемонов случайные папки
      let folders = shuffle(result).slice(0, hidden.length);
      console.log('Folders to hide', folders);

      let counter = 0;
      for (let pokemon of hidden) {
        fsPromise.writeFile(`${path}${folders[counter]}/pokemon.txt`, `${pokemon.name}|${pokemon.level}`);
        counter++;
      }
    })
    .then( () => console.log('Pokemons hidden:\n', hidden) )
    .then( () => new PokemonList(...hidden) )
    .catch(err => console.log(err));
};



const seek = (path) => {
  return fsPromise.readdir(path)
  .then(result => {
    let promiseArray = result.map(item => {

      if (item === 'pokemon.txt') {
        return fsPromise.readFile(path + '/' + item);

      } else {
          return new Promise((resolve, reject) => {
            fs.readFile(path + item + '/pokemon.txt', 'utf-8', (err, data) => {
              if (err) {
                // если файл не найден передаем в fullfilled промиса null
                return (err.code === 'ENOENT') ? resolve(null) : reject(err);
              }
              resolve(new Pokemon(...data.split('|')));
            });
          });
      }
    });
    // получаем промис, который ждет результатов завершения всех промисов из массива
    return Promise.all(promiseArray);

  })
  .then(result => result.filter(item => item !== null))
  .then(result => new PokemonList(...result))
  .then(result => console.log('Pokemons found:\n', result))
  .catch(err => console.log(err));
};

module.exports = {
  hide,
  seek
};
