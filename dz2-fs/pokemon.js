module.exports = class Pokemon {
  constructor(name, level) {
    this.name = name;
    this.level = level;
  }

  show() {
    console.log(`pokemon ${this.name} - level ${this.level}`);
  }
};
