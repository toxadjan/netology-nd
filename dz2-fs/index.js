const { hide , seek } = require('./hidenseek');
const input = process.argv[2];

switch (input) {
  case 'hide':
      hide(process.argv[3], require(process.argv[4]));
      break;
  case 'seek':
      seek(process.argv[3]);
      break;
  default:
      console.log('"node index hide ./field/ ./pokemons.json" - to hide pokemons\n"node index seek ./field/" - to seek pokemons ' );
      break;
}
