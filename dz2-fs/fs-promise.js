const fs = require('fs');

exports.mkdir = (name) => {
  return new Promise( (resolve, reject) => {
    fs.mkdir(name, err => {
      if (err) return reject(err);
      resolve();
    });
  });
};

exports.writeFile = (name, content) => {
  return new Promise ( (resolve, reject) => {
    fs.writeFile(name, content, { encoding: 'utf-8' }, err => {
      if (err) return reject(err);
      resolve();
    });
  });
};

exports.readdir = (name) => {
  return new Promise ( (resolve, reject) => {
    fs.readdir(name, (err, files) => {
      if (err) return reject(err);
      resolve(files);
    });
  });
};

exports.readFile = (name) => {
  return new Promise ( (resolve, reject) => {
    fs.readFile(name, { encoding: 'utf-8' }, (err, content) => {
      if (err) return reject(err);
      resolve(content);
    });
  });
};
