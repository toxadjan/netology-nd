module.exports = class PokemonList extends Array {
  add(name, level) {
    this.push(new Pokemon(name, level));
  }

  show() {
    this.forEach(item => console.log(item));
  }
};
