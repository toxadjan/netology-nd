const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const mongoose = require('mongoose');
const dbUrl = 'mongodb://localhost:27017/mongoose';

app.use(bodyParser.json());

app.listen(3000, () => {
  console.log('Listening on port 3000');
  mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false
  }, err => {
    if (err) return console.log(err);
    console.log('Connected to DB');
  });
});

const User = require('./models/user');
const Task = require('./models/task');


app.get('/users/', (req, res) => {
  User.find((error, result) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.json(result);
  });
});

app.post('/users/', (req, res) => {
  new User({
      name: req.body.name,
    })
    .save(function(error, result) {
      if (error)
        res.status(500).send({
          error: error.message,
          res: req.body
        });
      else
        res.json(result);
    });
});

app.get('/users/:name', (req, res) => {
  User.findOne({
    name: req.params.name
  }, (error, user) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.json(user);
  });
});

app.put('/users/:id', (req, res) => {
  let id = req.params.id;
  User.updateOne({
    "_id": id
  }, {
    $set: {
      name: req.body.name
    }
  }, function(error, user) {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.send('User deleted');
  });
});

app.delete('/users/:id', (req, res) => {
  let id = req.params.id;
  User.deleteOne({
    "_id": id
  }, (error) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.send('User deleted');
  });
});

app.get('/tasks/', (req, res) => {
  Task.find((error, tasks) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.json(tasks);
  });
});

app.post('/tasks/', (req, res) => {
  User.findOne({
    name: req.body.name
  }, (error, user) => {
    if (error)
      res.status(500).send({
        error: error.message
      });

    new Task({
        title: req.body.title,
        description: req.body.description,
        isOpen: true,
        user: user ? user._id : null
      })
      .save(function(err, result) {
        if (error)
          res.status(500).send({
            error: error.message
          });
        else
          res.json(result);
      });
  });
});

app.get('/tasks/:query', (req, res) => {
  Task.find({
    $or: [{
      title: req.params.query
    }, {
      description: req.params.query
    }]
  }, (error, result) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.json(result);
  });
});

app.put('/tasks/:id', (req, res) => {
  Task.findOne({
    _id: req.params.id
  }, (error, task) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    const data = {};
    if (req.body.user) {
      User.findOne({
        name: req.body.user
      }, (error, user) => {
        if (error)
          res.status(500).send({
            error: error.message
          });
        data.user = user._id;
        Object.assign(task, req.body, data);
        task.save((error, result) => {
          if (error)
            res.status(500).send({
              error: error.message
            });
          else
            res.json(result);
        });
      });
    } else {
      Object.assign(task, req.body);
      task.save((error, result) => {
        if (error)
          res.status(500).send({
            error: error.message
          });
        else
          res.json(result);
      });
    }
  });
});

app.delete('/tasks/:id', (req, res) => {
  Task.deleteOne({
    _id: req.params.id
  }, (error) => {
    if (error)
      res.status(500).send({
        error: error.message
      });
    else
      res.send('Task deleted');
  });
});
