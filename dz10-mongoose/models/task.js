let mongoose = require('mongoose');

let taskSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  date: {
    type: Date,
    default: Date.now()
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: false
  },
  isOpen: {
    type: Boolean,
    default: false
  }
});

let Task = mongoose.model('Task', taskSchema);

module.exports = Task;
