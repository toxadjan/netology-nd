let mongoose = require('mongoose');

let userSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  }
});

let User = mongoose.model('User', userSchema);

module.exports = User;
