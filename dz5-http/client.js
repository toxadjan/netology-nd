const http = require('http');
const querystring = require('querystring');

let data = querystring.stringify({
  firstname: 'Anton',
  lastname: 'Paulovich'
});

const options = {
  host: 'localhost',
  port: 3000,
  method: 'POST',
  path: '/',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': Buffer.byteLength(data)
  }
};

function process(data) {
  data = JSON.parse(data);
  return console.log(JSON.stringify(data, null, 3));
}

function handler(response) {
  let data = '';
  response.on('data', (chunk) => data += chunk);
  response.on('end', () => process(data));
}

const request = http.request(options);
request.on('error', (err) => console.log(err));
request.write(data);
request.on('response', handler);
request.end();
