const http = require('http');
const querystring = require('querystring');
const PORT = process.env.PORT || 3000;

process.on('unhandledRejection', (reason) => {
    console.log('Reason: ' + reason);
});

const options = {
  hostname: 'netology.tomilomark.ru',
  path: '/api/v1/hash',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  }
};

function parse(data, type) {
  switch (type) {
    case 'application/json':
      data = JSON.parse(data);
      break;
    case 'application/x-www-form-urlencoded':
      data = querystring.parse(data);
      break;
  }

  return data;
}

function getHash(data) {
  return new Promise( (resolve, reject) => {
    const request = http.request(options);
    request.on('error', (err) => reject(err));
    if (request.statusCode === 404) {
      reject(request.statusCode);
    }
    request.write(JSON.stringify(data));

    request.on('response', response => {
      let content = '';
      response.on('data', function (chunk) {
        content += chunk;
      });
      response.on('end', function () {
        let type = response.headers['content-type'].split(';').shift();
        content = parse(content, type);
        resolve(content);
      });
    });
    request.end();
  });
}

function handler(req, res) {
  let data = '';
  req.on('data', chunk => data += chunk);
  req.on('end', () => {
    let type = req.headers['content-type'].split(';').shift();
    data = parse(data, type);
    getHash(data)
      .then( (result) => {
        data = Object.assign(data, result);
        res.writeHead(200, 'OK', {'Content-Type': 'application/json'});
        res.write(JSON.stringify(data));
        res.end();
      })
      .catch(err => {
        res.end(err);
      });
  });
}

const server = http.createServer();
server.on('error', (err) => console.log(err));
server.on('request', handler);
server.on('listening', () => console.log(`Running server at ${PORT}`));
server.listen(PORT);
