﻿##Основное задание

https://github.com/webmaxru/netology-angular-pokemons/tree/xhr

- Добавить лоадеры в список покемонов и ягод.
- Зарегистрироваться на Backendless.com, создать приложение, переподключить на свои application-id и secret-key.
- Установить эти два заголовка через $http.defaults.headers (для всех запросов).
- Сделать интерфейс редактирования покемона (на основе createPokemon) и сделать запрос PUT при клике на кнопку «Сохранить» (подключать backend не нужно).

##На зачет с отличием

- Реализовать возможность подключения к разным комнатам.
