'use strict';

pokemonApp.controller('EditPokemonCtrl', function($scope, PokemonsService) {

    $scope.editPokemon = {};

    $scope.deletePokemon = function(myPokemon) {

        $scope.deletionSuccess = false;
        $scope.editionError = false;

        PokemonsService.deletePokemon(myPokemon).then(function(response) {

            $scope.editPokemon = {};
            $scope.deletionSuccess = true;
            $scope.editionSuccess = false;

        })
        .catch(function(error) {
            $scope.editionError = true;
            console.log(error);
        });

    };

    $scope.changePokemon = function(pokemonID, myPokemon) {

        $scope.editionSuccess = false;
        $scope.deletionSuccess = false;

        PokemonsService.editPokemon(pokemonID, myPokemon).then(function(response) {

            $scope.editPokemon = {};
            $scope.editionSuccess = true;
        })
        .catch(function(error) {
            $scope.editionError = true;
            console.log(error);
        });
    };

});
