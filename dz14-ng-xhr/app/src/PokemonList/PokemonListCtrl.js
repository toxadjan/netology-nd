'use strict';

pokemonApp.controller('PokemonListCtrl', function($scope, $q, PokemonsService, BerriesService) {

    // PokemonsService.getPokemons().then(function(response) {
    //     $scope.pokemons = response.data.results;
    // });
    //
    // BerriesService.getBerries().then(function(response) {
    //     $scope.berries = response.data.results;
    // });

    // $scope.pokemonLoaded = false;
    // $scope.berriesLoaded = false;
    //
    // PokemonsService.getPokemons().then(function(response) {
    //     $scope.pokemons = response.data.results;
    //     $scope.pokemonLoaded = true;
    //
    //     return BerriesService.getBerries();
    //
    // }).then(function(response) {
    //     $scope.berries = response.data.results;
    //     $scope.berriesLoaded = true;
    // });

    $q.all({
        obj1: PokemonsService.getPokemons(),
        obj2: BerriesService.getBerries()

    }).then(function(values){
        $scope.pokemons = values.obj1.response.data.results;
        $scope.berries = values.obj2.response.data.results;
        $scope.pokemonLoaded = true;
        $scope.berriesLoaded = true;
        
    }).catch(function(error){
        console.log(error);
    });

});
